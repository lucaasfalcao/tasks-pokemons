import fetch from "node-fetch"

export const justName = async (key) =>{
    function pokemonsUrl (key){
        const url = 'https://pokeapi.co/api/v2/type/';
        return `${url}${key}`
    }
    const response = await fetch(pokemonsUrl(key));
    const pokemonJson = await response.json();
    const pokemonList = pokemonJson.pokemon.map(x => x.pokemon);
    const pokemonName = pokemonList.map(y => y.name);
    return pokemonName;
}

export const justUrl = async (key) =>{
    function pokemonsUrl (key){
        const url = 'https://pokeapi.co/api/v2/type/';
        return `${url}${key}`
    }
    const response = await fetch(pokemonsUrl(key));
    const pokemonJson = await response.json();
    const pokemonList = pokemonJson.pokemon.map(x => x.pokemon);
    const pokemonUrl = pokemonList.map(z => z.url);
    return pokemonUrl;
}
