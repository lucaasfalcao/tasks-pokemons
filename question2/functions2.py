import requests

def just_name(key):
    response = requests.get('https://pokeapi.co/api/v2/type/'+key)
    data = response.json()
    pokemon_data_slot = data['pokemon']
    pokemon_data = []
    for i in pokemon_data_slot:
        pokemon_data.append(i['pokemon']['name'])
    return pokemon_data

def just_url(key):
    response = requests.get('https://pokeapi.co/api/v2/type/'+key)
    data = response.json()
    pokemon_data_slot = data['pokemon']
    pokemon_data = []
    for i in pokemon_data_slot:
        pokemon_data.append(i['pokemon']['url'])
    return pokemon_data