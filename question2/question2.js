import { justName, justUrl } from "./functions2.js";

const thisUrl = async(key1, key2) =>{
    const names = await justName (key1);
    const url = await justUrl (key1);
    const thisPokemon = key2
    for (let i = 0; i<names.length; i++){
        if (names[i]==thisPokemon){
            const array = url[i].split('/')
            console.log(`${url[i]}), ${array[6]}, ${i}`)
        }
    }
}

thisUrl('rock', 'onix');