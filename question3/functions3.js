import fetch from "node-fetch"

export const pokemonsNameUrl = async (key) =>{
    function pokemonsUrl (key){
        const url = 'https://pokeapi.co/api/v2/type/';
        return `${url}${key}`
    }
    const response = await fetch(pokemonsUrl(key));
    const pokemonJson = await response.json();
    const pokemonList = pokemonJson.pokemon.map(x => x.pokemon);
    return pokemonList;
}

export const justName = async (key) =>{
    function pokemonsUrl (key){
        const url = 'https://pokeapi.co/api/v2/type/';
        return `${url}${key}`
    }
    const response = await fetch(pokemonsUrl(key));
    const pokemonJson = await response.json();
    const pokemonList = pokemonJson.pokemon.map(x => x.pokemon);
    const pokemonName = pokemonList.map(y => y.name);
    return pokemonName;
}

export const addPokemon = async (pokemonData, pokemonName, thisPokemon) =>{
    let aux = 0;
    for (let i = 0; i<pokemonName.length; i++){
        if (pokemonName[i] == thisPokemon.name){
            console.log('Error: duplicate pokemons!');
            aux = aux+1
        }
    }
    if (aux == 0){
        pokemonData.push(thisPokemon);
        console.log(pokemonData);
    }
}
