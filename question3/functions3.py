import requests

def pokemon_name_url(key):
    response = requests.get('https://pokeapi.co/api/v2/type/'+key)
    data = response.json()
    pokemon_data_slot = data['pokemon']
    pokemon_data = []
    for i in pokemon_data_slot:
        pokemon_data.append(i['pokemon'])
    return pokemon_data

def just_name(key):
    response = requests.get('https://pokeapi.co/api/v2/type/'+key)
    data = response.json()
    pokemon_data_slot = data['pokemon']
    pokemon_data = []
    for i in pokemon_data_slot:
        pokemon_data.append(i['pokemon']['name'])
    return pokemon_data

def add_pokemon(pokemon_data, pokemon_name, this_pokemon):
    aux = 0
    for i in range(len(pokemon_name)):
        if pokemon_name[i] == this_pokemon['name']:
            print('Error: duplicate pokemons!')
            aux = aux+1

    if aux == 0:
        pokemon_data.append(this_pokemon)
        print(pokemon_data)

