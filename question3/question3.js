import { pokemonsNameUrl, justName, addPokemon } from "./functions3.js";

const addThisPokemon = async (key1, key2) =>{
    const yourData = await pokemonsNameUrl(key1);
    const yourName = await justName(key1);
    await addPokemon(yourData, yourName, key2);
}

const newPokemon = {
    name: 'falcon',
    url: 'https://pokeapi.co/api/v2/pokemon/17/'
}

addThisPokemon('rock', newPokemon);