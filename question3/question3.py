from functions3 import pokemon_name_url, just_name, add_pokemon

def add_this_pokemon(key1, key2):
    your_data = pokemon_name_url(key1)
    your_name = just_name(key1)
    add_pokemon(your_data, your_name, key2)

new_pokemon = {'name': 'falcon', 'url': 'https://pokeapi.co/api/v2/pokemon/17/'}

add_this_pokemon('rock', new_pokemon)



