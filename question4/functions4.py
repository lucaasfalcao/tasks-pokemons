import requests

def pokemon_name_url(key):
    response = requests.get('https://pokeapi.co/api/v2/type/'+key)
    data = response.json()
    pokemon_data_slot = data['pokemon']
    pokemon_data = []
    for i in pokemon_data_slot:
        pokemon_data.append(i['pokemon'])
    return pokemon_data

def just_name(key):
    response = requests.get('https://pokeapi.co/api/v2/type/'+key)
    data = response.json()
    pokemon_data_slot = data['pokemon']
    pokemon_data = []
    for i in pokemon_data_slot:
        pokemon_data.append(i['pokemon']['name'])
    return pokemon_data

def delete_pokemon(pokemon_data, pokemon_name, this_pokemon):
    for i in range(len(pokemon_name)):
        if pokemon_name[i] == this_pokemon:
            del pokemon_data[i]
            print(pokemon_data)