import { pokemonsNameUrl, justName, deletePokemon } from "./functions4.js";

const deleteThisPokemon = async (key1, key2) =>{
    const yourData = await pokemonsNameUrl(key1);
    const yourName = await justName(key1);
    await deletePokemon(yourData, yourName, key2);
}

deleteThisPokemon('rock', 'onix');