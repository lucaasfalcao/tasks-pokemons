from functions4 import pokemon_name_url, just_name, delete_pokemon

def delete_this_pokemon(key1, key2):
    your_data = pokemon_name_url(key1)
    your_name = just_name(key1)
    delete_pokemon(your_data, your_name, key2)

delete_this_pokemon('rock', 'onix')