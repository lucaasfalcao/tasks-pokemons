import fetch from "node-fetch"

export const pokemonsNameUrl = async (key) =>{
    function pokemonsUrl (key){
        const url = 'https://pokeapi.co/api/v2/type/';
        return `${url}${key}`
    }
    const response = await fetch(pokemonsUrl(key));
    const pokemonJson = await response.json();
    const pokemonList = pokemonJson.pokemon.map(x => x.pokemon);
    return pokemonList;
}

export const sortPokemons = async (pokemonData, orderCriterion) =>{
    if (orderCriterion === 'crescente'){
        console.log(pokemonData.sort((x, y) => x.name.localeCompare(y.name)));
    }
    else if (orderCriterion === 'decrescente'){
        console.log(pokemonData.sort((x, y) => y.name.localeCompare(x.name)));
    }
}