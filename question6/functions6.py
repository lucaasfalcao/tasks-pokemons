import requests

def pokemon_name_url(key):
    response = requests.get('https://pokeapi.co/api/v2/type/'+key)
    data = response.json()
    pokemon_data_slot = data['pokemon']
    pokemon_data = []
    for i in pokemon_data_slot:
        pokemon_data.append(i['pokemon'])
    return pokemon_data

def sort_pokemons(pokemon_data, order_criterion):
    if order_criterion == 'crescente':
        sorted_data = sorted(pokemon_data)
        print(sorted_data)
    elif order_criterion == 'decrescente':
        sorted_data = sorted(pokemon_data, reverse = True)
        print(sorted_data)
