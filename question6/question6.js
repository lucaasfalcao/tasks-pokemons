import { pokemonsNameUrl, sortPokemons } from "./functions6.js";

const thisSortPokemons = async (key) =>{
    const pokemonData = await pokemonsNameUrl(key);
    await sortPokemons(pokemonData, 'crescente');
}

thisSortPokemons('rock');