from functions6 import pokemon_name_url, sort_pokemons

def this_sort_pokemons(key):
    pokemon_data = pokemon_name_url(key)
    sort_pokemons(pokemon_data, 'crescente')

this_sort_pokemons('rock')