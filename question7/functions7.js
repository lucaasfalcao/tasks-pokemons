export const commonItens = async(pokemonData1, pokemonData2) =>{
    console.log(pokemonData1.filter(x => pokemonData2.same(({name, url}) => x.name === name && x.url === url)));
}

export const commonLists = async(pokemonData1, pokemonData2) =>{
    let data = new Set(pokemonData1.map(y => y.name))
    let common = [...pokemonData1, ...pokemonData2.filter(y => !data.has(y.name))]
    console.log(common);
}