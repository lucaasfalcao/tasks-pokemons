import { commonItens, commonLists } from "./functions7.js";

const thisCommonItens = async(key1, key2) =>{
    const itens = await commonItens(key1, key2);
    await commonLists(itens, key2);
}

thisCommonItens('flying', 'dragon')